//
//  UIFont+FontKit.swift
//  Coop Trainer App
//
//  Created by Rogier van den Brink on 08/05/2019.
//  Copyright © 2019 Coop. All rights reserved.
//

import UIKit

extension UIFont {
    open class func museo500Font(ofSize fontSize: CGFloat) -> UIFont {
        return UIFont(name: "Museo500-Regular", size: fontSize)!
    }
    
    open class func museo700Font(ofSize fontSize: CGFloat) -> UIFont {
        return UIFont(name: "Museo-700", size: fontSize)!
    }
    
    open class func museo300Font(ofSize fontSize: CGFloat) -> UIFont {
        return UIFont(name: "Museo300-Regular", size: fontSize)!
    }
    
    open class func openSansFont(ofSize fontSize: CGFloat) -> UIFont {
        return UIFont(name: "OpenSans-Regular", size: fontSize)!
    }
    
    open class func openSansBoldFont(ofSize fontSize: CGFloat) -> UIFont {
        return UIFont(name: "OpenSans-Bold", size: fontSize)!
    }
    
    open class func openSansSemiBoldFont(ofSize fontSize: CGFloat) -> UIFont {
        return UIFont(name: "OpenSans-Semibold", size: fontSize)!
    }
    
    open class func gunplayRegularFont(ofSize fontSize: CGFloat) -> UIFont {
        return UIFont(name: "Gunplay-Regular", size: fontSize)!
    }
    
    open class func ioniconsFont(ofSize fontSize: CGFloat) -> UIFont {
        return UIFont(name: "ionicons", size: fontSize)!
    }
}
