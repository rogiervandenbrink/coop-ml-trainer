//
//  ConnectionManager.swift
//  Coop Trainer App
//
//  Created by Rogier van den Brink on 10/05/2019.
//  Copyright © 2019 Coop. All rights reserved.
//

import Foundation

class ConnectionManager {
    
    static let sharedInstance = ConnectionManager()
    private var reachability: Reachability!
    
    func observeReachability() {
        self.reachability = Reachability()
        NotificationCenter.default.addObserver(self, selector: #selector(self.reachabilityChanged), name: NSNotification.Name.reachabilityChanged, object: nil)
        do {
            try self.reachability.startNotifier()
        } catch let error {
            print("Error occured while starting reachability notifications : \(error.localizedDescription)")
        }
    }
    
    @objc func reachabilityChanged(note: Notification) {
        let reachability = note.object as! Reachability
        switch reachability.connection {
        case .cellular:
            print("Network available via Cellular Data.")
        case .wifi:
            print("Network available via WiFi.")
        case .none:
            Alert.simpleAlert(message: NSLocalizedString("internet_error_message", comment: ""), title: "")
        }
    }
}
