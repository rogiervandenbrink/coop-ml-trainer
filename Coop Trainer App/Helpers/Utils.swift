//
//  Utils.swift
//  Coop Trainer App
//
//  Created by Rogier van den Brink on 10/05/2019.
//  Copyright © 2019 Coop. All rights reserved.
//

import UIKit

extension UIView {
    func dropShadow(height: CGFloat = 4.0) {
        self.layer.shadowColor = UIColor.lightGray.cgColor
        self.layer.shadowOffset = CGSize(width: 1.0, height: height)
        self.layer.shadowOpacity = 0.6
        self.layer.shadowRadius = 8.0
        self.layer.masksToBounds = false
    }
    
    func roundedCorners() {
        self.layer.cornerRadius = 3.0
    }
}

extension UIViewController {
    func addShadow() {
        if let navCtrl = self.navigationController {
            navCtrl.navigationBar.layer.shadowColor = UIColor.black.cgColor
            navCtrl.navigationBar.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
            navCtrl.navigationBar.layer.shadowRadius = 6.0
            navCtrl.navigationBar.layer.shadowOpacity = 0.8
            navCtrl.navigationBar.layer.masksToBounds = false
        }
    }
    
    func hideBackTitle() {
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
}

extension UITextField {
    
    @IBInspectable var doneAccessory: Bool {
        get {
            return self.doneAccessory
        }
        set (hasDone) {
            if hasDone {
                addDoneButtonOnKeyboard()
            }
        }
    }
    
    func addDoneButtonOnKeyboard() {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 50))
        doneToolbar.barStyle = .default
        
        let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: NSLocalizedString("confirm_title", comment: ""), style: .done, target: self, action: #selector(self.doneButtonAction))
        
        let items = [flexSpace, done]
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        
        self.inputAccessoryView = doneToolbar
    }
    
    @objc func doneButtonAction() {
        self.resignFirstResponder()
    }
    
}
