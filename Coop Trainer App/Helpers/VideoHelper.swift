//
//  VideoHelper.swift
//  Coop Trainer App
//
//  Created by Rogier van den Brink on 09/05/2019.
//  Copyright © 2019 Coop. All rights reserved.
//

import Foundation
import UIKit
import AVFoundation

public class VideoHelper {
    
    let session = AVCaptureSession()
    let sessionQueue = DispatchQueue(label: "session queue", attributes: [])
    var movieFileOutput: AVCaptureMovieFileOutput?
    var backgroundRecordingID: UIBackgroundTaskIdentifier? = nil
    var isVideoRecording: Bool = false
    var outputFilePath: String?
    
    public func recordProduct(sku: String, recordingDelegate: AVCaptureFileOutputRecordingDelegate) {
        guard let movieFileOutput = self.movieFileOutput else {
            return
        }
        
        sessionQueue.async { [unowned self] in
            if !movieFileOutput.isRecording {
                if UIDevice.current.isMultitaskingSupported {
                    self.backgroundRecordingID = UIApplication.shared.beginBackgroundTask(expirationHandler: nil)
                }
                
                // Start recording to a temporary file.
                let outputFileName = sku
                let outputFilePath = (NSTemporaryDirectory() as NSString).appendingPathComponent((outputFileName as NSString).appendingPathExtension("mov")!)
                movieFileOutput.startRecording(to: URL(fileURLWithPath: outputFilePath), recordingDelegate: recordingDelegate)
                self.outputFilePath = outputFilePath
                self.isVideoRecording = true
                
            }
            else {
                movieFileOutput.stopRecording()
            }
        }
    }
    
    public func stopVideoRecording() {
        
        if self.isVideoRecording {
            self.isVideoRecording = false
            movieFileOutput!.stopRecording()
            print("recording stopped")
        }
    }
    
    func encodeVideo(videoUrl: URL, outputUrl: URL? = nil, completionHandler: @escaping (URL?) -> Void) {

        var finalOutputUrl: URL? = outputUrl
        
        if finalOutputUrl == nil {
            var url = videoUrl
            url.deletePathExtension()
            url.appendPathExtension(".mp4")
            finalOutputUrl = url
        }
        
        if FileManager.default.fileExists(atPath: finalOutputUrl!.path) {
            print("Converted file already exists \(finalOutputUrl!.path)")
            completionHandler(finalOutputUrl)
            return
        }
        
        let asset = AVURLAsset(url: videoUrl)
        if let exportSession = AVAssetExportSession(asset: asset, presetName: AVAssetExportPresetPassthrough) {
            exportSession.outputURL = finalOutputUrl!
            exportSession.outputFileType = AVFileType.mp4
            let start = CMTimeMakeWithSeconds(0.0, preferredTimescale: 0)
            let range = CMTimeRangeMake(start: start, duration: asset.duration)
            exportSession.timeRange = range
            exportSession.shouldOptimizeForNetworkUse = true
            exportSession.exportAsynchronously() {
                
                switch exportSession.status {
                case .failed:
                    print("Export failed: \(exportSession.error != nil ? exportSession.error!.localizedDescription : "No Error Info")")
                    Alert.simpleAlert(message: exportSession.error!.localizedDescription, title: "Fout bij het encoderen van de video")
                case .cancelled:
                    Alert.simpleAlert(message: "Het encoderen is geannuleerd.", title: "Fout bij het encoderen van de video")
                    print("Export canceled")
                case .completed:
                    completionHandler(finalOutputUrl!)
                default:
                    break
                }
            }
        } else {
            completionHandler(nil)
        }
    }
    
    func deleteFile(_ filePath: URL) {
        guard FileManager.default.fileExists(atPath: filePath.path) else{
            return
        }
        do {
            try FileManager.default.removeItem(atPath: filePath.path)
        }catch{
            fatalError("Unable to delete file: \(error) : \(#function).")
        }
    }
}
