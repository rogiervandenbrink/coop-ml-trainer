//
//  UIColor+ColorKit.swift
//  Coop Trainer App
//
//  Created by Rogier van den Brink on 08/05/2019.
//  Copyright © 2019 Coop. All rights reserved.
//

import UIKit

extension UIColor {
    @nonobjc class var coopOrange: UIColor {
        return UIColor(red: 247.0 / 255.0, green: 147.0 / 255.0, blue: 0.0, alpha: 1.0)
    }
    
    @nonobjc class var coopGrey: UIColor {
        return UIColor(white: 159.0 / 255.0, alpha: 1.0)
    }
    
    @nonobjc class var coopBackground: UIColor {
        return UIColor(red: 243.0 / 255.0, green: 239.0 / 255.0, blue: 235.0 / 255.0, alpha: 1.0)
    }
    
    @nonobjc class var coopBlack: UIColor {
        return UIColor(white: 51.0 / 255.0, alpha: 1.0)
    }
    
    @nonobjc class var coopDeepBlack: UIColor {
        return UIColor(white: 20.0 / 255.0, alpha: 1.0)
    }
    
    @nonobjc class var coopQuantityGrey: UIColor {
        return UIColor(red: 169.0 / 255.0, green: 165.0 / 255.0, blue: 162.0 / 255.0, alpha: 1.0)
    }
    
    @nonobjc class var coopRed: UIColor {
        return UIColor(red: 227.0 / 255.0, green: 6.0 / 255.0, blue: 19.0 / 255.0, alpha: 1.0)
    }
    
    @nonobjc class var grossGreen: UIColor {
        return UIColor(red: 156.0 / 255.0, green: 190.0 / 255.0, blue: 20.0 / 255.0, alpha: 1.0)
    }
    
    @nonobjc class var coopBorderGrey: UIColor {
        return UIColor(white: 216.0 / 255.0, alpha: 1.0)
    }
    
    @nonobjc class var coopWhiteTwo: UIColor {
        return UIColor(white: 216.0 / 255.0, alpha: 1.0)
    }
    
    @nonobjc class var coopFormErrorBackground: UIColor {
        return UIColor(red: 254.0 / 255.0, green: 230.0 / 255.0, blue: 191.0 / 255.0, alpha: 1.0)
    }
    
    @nonobjc class var coopGreyBackground: UIColor {
        return UIColor(red: 247.0 / 255.0, green: 247.0 / 255.0, blue: 247.0 / 255.0, alpha: 1.0)
    }
    
    @nonobjc class var butterscotch: UIColor {
        return UIColor(red: 253.0 / 255.0, green: 174.0 / 255.0, blue: 58.0 / 255.0, alpha: 1.0)
    }
    
    @nonobjc class var warmGrey: UIColor {
        return UIColor(white: 155.0 / 255.0, alpha: 1.0)
    }
    
    @nonobjc class var whiteThree: UIColor {
        return UIColor(white: 240.0 / 255.0, alpha: 1.0)
    }
    
    @nonobjc class var greyishBrown: UIColor {
        return UIColor(white: 66.0 / 255.0, alpha: 1.0)
    }
    
    @nonobjc class var breakfastYellow: UIColor {
        return UIColor(red: 0.99, green: 0.78, blue: 0.06, alpha: 0.7)
    }
    
    @nonobjc class var lunchOrange: UIColor {
        return UIColor(red: 0.96, green: 0.43, blue: 0.24, alpha: 0.7)
    }
    
    @nonobjc class var snackGreen: UIColor {
        return UIColor(red: 0.25, green: 0.77, blue: 0.42, alpha: 0.7)
    }
    
    @nonobjc class var amusePurple: UIColor {
        return UIColor(red: 0.63, green: 0.38, blue: 0.89, alpha: 0.7)
    }
    
    @nonobjc class var starterBlue: UIColor {
        return UIColor(red: 0.36, green: 0.63, blue: 0.91, alpha: 0.7)
    }
    
    @nonobjc class var mainDishGreen: UIColor {
        return UIColor(red: 0.08, green: 0.77, blue: 0.69, alpha: 0.7)
    }
    
    @nonobjc class var sideDishGreen: UIColor {
        return UIColor(red: 0.53, green: 0.73, blue: 0.16, alpha: 0.7)
    }
    
    @nonobjc class var dessertPink: UIColor {
        return UIColor(red: 0.96, green: 0.13, blue: 0.44, alpha: 0.7)
    }
    
    @nonobjc class var drinkRed: UIColor {
        return UIColor(red: 0.95, green: 0.26, blue: 0.13, alpha: 0.7)
    }
    
    @nonobjc class var whiteFour: UIColor {
        return UIColor(white: 247.0 / 255.0, alpha: 1.0)
    }
    
    @nonobjc class var transparentWhite: UIColor {
        return UIColor(red: 255.0 / 255.0, green: 255.0 / 255.0, blue: 255.0 / 255.0, alpha: 0.7)
    }
    
    @nonobjc class var homeSearchButtonOffWhite: UIColor {
        return UIColor(red: 254.0 / 255.0, green: 243.0 / 255.0, blue: 224.0 / 255.0, alpha: 1.0)
    }
}
