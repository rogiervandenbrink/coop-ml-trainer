//
//  StyleKit.swift
//  Coop Trainer App
//
//  Created by Rogier van den Brink on 08/05/2019.
//  Copyright © 2019 Coop. All rights reserved.
//
import UIKit

public class StyleKit: NSObject {
    
    public static func closeIconView(frame: CGRect, color: UIColor) -> UIView {
        let imageOfCloseIcon = UIView(frame: frame)
        imageOfCloseIcon.backgroundColor = UIColor.clear
        
        let startPoint1 = CGPoint(x: 0, y: frame.size.height)
        let endPoint1 = CGPoint(x: frame.size.width, y: 0)
        let line1 = CAShapeLayer()
        let linePath1 = UIBezierPath()
        linePath1.move(to: startPoint1)
        linePath1.addLine(to: endPoint1)
        line1.path = linePath1.cgPath
        line1.strokeColor = color.cgColor
        line1.lineWidth = 2
        line1.lineJoin = CAShapeLayerLineJoin.round
        imageOfCloseIcon.layer.addSublayer(line1)
        
        let startPoint2 = CGPoint(x: 0, y: 0)
        let endPoint2 = CGPoint(x: frame.size.width, y: frame.size.height)
        let line2 = CAShapeLayer()
        let linePath2 = UIBezierPath()
        linePath2.move(to: startPoint2)
        linePath2.addLine(to: endPoint2)
        line2.path = linePath2.cgPath
        line2.strokeColor = color.cgColor
        line2.lineWidth = 2
        line2.lineJoin = CAShapeLayerLineJoin.round
        imageOfCloseIcon.layer.addSublayer(line2)
        
        return imageOfCloseIcon
    }
    
}
