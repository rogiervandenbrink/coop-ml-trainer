//
//  Alerts.swift
//  Coop Trainer App
//
//  Created by Rogier van den Brink on 09/05/2019.
//  Copyright © 2019 Coop. All rights reserved.
//

import Foundation
import UIKit
import Bond
import ReactiveKit

public enum AlertType: String {
    case notice = "alerttype.notice"
    case retry = "alerttype.retry"
}

public class Alert {
    
    static var alertView: AlertView?
    static var progressAlertView: ProgressAlertView?
    static let disposeBag = DisposeBag()
    
    public static func simpleAlert(message: String, title: String = "") {
        // Make sure this is executed on the main queue
        DispatchQueue.main.async {
            // Create custom alert
            if let keyWindow = UIApplication.shared.keyWindow {
                let alertView = AlertView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height), title: title, message: message, buttonTitle: "Ok")
                keyWindow.addSubview(alertView)
                
                alertView.present(completionHandler: { (_) in
                    
                })
            }
        }
    }
    
    public static func simpleAlertWithButton(message: String, title: String, buttonTitle: String = "Ok", completionHandler: @escaping (Bool) -> Void) {
        // Make sure this is executed on the main queue
        DispatchQueue.main.async {
            // Create custom alert
            if let keyWindow = UIApplication.shared.keyWindow {
                
                let alertView = AlertView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height), title: title, categoryTitle: "", message: "", buttonTitle: buttonTitle)
                keyWindow.addSubview(alertView)
                
                alertView.present(completionHandler: { (bool) in
                    completionHandler(bool)
                })
            }
        }
    }
    
    public static func inputAlert(title: String, placeholder: String, buttonTitle: String = "Ok", completionHandler: @escaping (Bool) -> Void) {
        // Make sure this is executed on the main queue
        DispatchQueue.main.async {
            // Create custom alert
            if let keyWindow = UIApplication.shared.keyWindow {
                
                let alertView = InputAlertView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height), title: title, placeHolder: placeholder, buttonTitle: buttonTitle)
                keyWindow.addSubview(alertView)
                
                alertView.present(completionHandler: { (bool) in
                    completionHandler(bool)
                })
            }
        }
    }
    
    public static func progressAlert(title: String = "", buttonTitle: String = "OK", completionHandler: @escaping (Bool) -> Void) {
        // Make sure this is executed on the main queue
        DispatchQueue.main.async {
            // Create custom alert
            if let keyWindow = UIApplication.shared.keyWindow {
                
                let alertView = ProgressAlertView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height), title: title, categoryTitle: "", message: "", buttonTitle: buttonTitle)
                keyWindow.addSubview(alertView)
                self.progressAlertView = alertView
                
                alertView.present(completionHandler: { (bool) in
                    completionHandler(bool)
                })
            }
        }
    }
    
    public static func setProgress(progress: Float) {
        guard let progressAlertView = self.progressAlertView else { return }
        
        progressAlertView.setProgress(progress: progress)
    }
}
