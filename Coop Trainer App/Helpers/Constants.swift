//
//  Constants.swift
//  Coop Trainer App
//
//  Created by Rogier van den Brink on 17/05/2019.
//  Copyright © 2019 Coop. All rights reserved.
//

import Foundation

struct Constants {
    public static let emailAdress = "emailaddress"
    public static let usersMlTrainer = "users_mltrainer"
    public static let productsFilmed = "products_filmed"
    public static let productCount = "products_count"
}
