//
//  ProductViewController.swift
//  Coop Trainer App
//
//  Created by Rogier van den Brink on 08/05/2019.
//  Copyright © 2019 Coop. All rights reserved.
//

import UIKit
import AVFoundation

class ProductViewController: UIViewController {

    @IBOutlet weak var closeButton: UIButton?
    @IBOutlet var previewView: UIImageView?
    @IBOutlet var playButton: UIButton?
    
    @IBOutlet var progressView: UIProgressView?
    
    var firestorageService = FirestorageService()
    var videoHelper = VideoHelper()
    var isRecording: Bool = false
    var timer = Timer()
    var progressTimer = Timer()
    var recordTime = 8.0
    
    var isProduct: Bool = true
    var skuLabel: UILabel?
    
    var sku: String? {
        didSet {
            updateProperties()
        }
    }
    
    var session: AVCaptureSession?
    
    override func viewDidLoad() {
        setup()
    }
    
    private func setup() {
        let closeButton = UIButton(frame: CGRect(x: 13, y: 6 + 30, width: 50, height: 50))
        closeButton.setImage(UIImage(named: "delete28X28"), for: [])
        self.view.addSubview(closeButton)
        self.closeButton = closeButton
        closeButton.reactive.tap.observeNext { [unowned self] in
            
            self.closeScanner()
            
        }.dispose(in: reactive.bag)
        
        let session = AVCaptureSession()
        self.session = session
        
        session.sessionPreset = AVCaptureSession.Preset.photo
        let captureDevice = AVCaptureDevice.default(for: AVMediaType.video)
        
        let deviceInput = try! AVCaptureDeviceInput(device: captureDevice!)
        let deviceOutput = AVCaptureVideoDataOutput()
        deviceOutput.videoSettings = [kCVPixelBufferPixelFormatTypeKey as String: Int(kCVPixelFormatType_32BGRA)]
        session.addInput(deviceInput)
        
        let previewView = UIImageView(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height - 180))
        self.view.addSubview(previewView)
        self.previewView = previewView
        
        let skuLabel = UILabel(frame: CGRect(x: 30, y: 80, width: self.view.frame.size.width - 60, height: 50))
        skuLabel.font = UIFont.openSansBoldFont(ofSize: 17)
        skuLabel.textColor = .white
        skuLabel.textAlignment = .center
        skuLabel.text = NSLocalizedString("sku_placeholder_text", comment: "")
        self.view.addSubview(skuLabel)
        self.skuLabel = skuLabel
        
        let bottomView = UIView(frame: CGRect(x: 0, y: self.view.frame.size.height - 180, width: self.view.frame.size.width, height: 160))
        bottomView.backgroundColor = .white
        self.view.addSubview(bottomView)
        
        let progressView = UIProgressView(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 10))
        progressView.progressViewStyle = .bar
        progressView.transform = CGAffineTransform.init(scaleX: 1.0, y: 4.0)
        progressView.setProgress(0.0, animated: true)
        progressView.trackTintColor = UIColor.lightGray
        progressView.tintColor = UIColor.coopOrange
        bottomView.addSubview(progressView)
        self.progressView = progressView
        
        let playButton = UIButton(frame: CGRect(x: (self.view.frame.size.width / 2) - 35, y: 23, width: 70, height: 70))
        playButton.setImage(UIImage(named: "play"), for: [])
        playButton.layer.cornerRadius = playButton.frame.size.height / 2
        playButton.imageEdgeInsets = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
        playButton.backgroundColor = UIColor.coopOrange
        bottomView.addSubview(playButton)
        self.playButton = playButton
        
        playButton.reactive.tap.observeNext { [unowned self] (_) in
            if let sku = self.sku,
                !self.isRecording {
                
                self.recordProduct(sku: sku)
            } else {
                
                self.stopRecording()
            }
        }.dispose(in: reactive.bag)
        
        let instructionLabel = UILabel(frame: CGRect(x: 0, y: 106, width: self.view.frame.size.width, height: 60))
        instructionLabel.textAlignment = .center
        instructionLabel.textColor = .black
        instructionLabel.font = UIFont.museo500Font(ofSize: 15)
        instructionLabel.text = NSLocalizedString("instruction_text", comment: "")
        instructionLabel.lineBreakMode = .byWordWrapping
        instructionLabel.numberOfLines = 0
        bottomView.addSubview(instructionLabel)
        
        let imageLayer = AVCaptureVideoPreviewLayer(session: session)
        imageLayer.frame = previewView.bounds
        imageLayer.videoGravity = .resizeAspectFill
        previewView.layer.addSublayer(imageLayer)
        
        configureVideoOutput()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        startLiveVideo()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        stopSession()
    }
    
    func configureVideoOutput() {
        let movieFileOutput = AVCaptureMovieFileOutput()
        
        guard let session = self.session else { return }
        
        if session.canAddOutput(movieFileOutput) {
            session.addOutput(movieFileOutput)
            if let connection = movieFileOutput.connection(with: AVMediaType.video) {
                if connection.isVideoStabilizationSupported {
                    connection.preferredVideoStabilizationMode = .auto
                }
            }
            self.videoHelper.movieFileOutput = movieFileOutput
        }
    }
    
    func stopSession() {
        if let session = self.session,
            session.isRunning {
            DispatchQueue.global().async {
                session.stopRunning()
            }
        }
    }
    
    func restartSession() {
        if let session = self.session,
            session.isRunning {
            
            session.stopRunning()
            
            startLiveVideo()
        }
    }
    
    private func startLiveVideo() {
        
        if let session = self.session {
            session.startRunning()
        }
    }
    
    func closeScanner() {
        self.dismiss(animated: true) {
            // dismiss
        }
    }
    
    func updateProperties() {
        guard let skuLabel = self.skuLabel,
            let sku = self.sku else { return }
        
        skuLabel.text = "\(NSLocalizedString("sku_placeholder_text", comment: ""))\(sku)"
        print("isProduct: \(self.isProduct)")
        
        //checkIfProductExists()
    }
    
    func checkIfProductExists() {
        guard let sku = self.sku else { return }
        self.firestorageService.checkIfAlreadyScanned(uniqueId: sku, isProduct: self.isProduct) { (exists) in
            if exists {
                switch self.isProduct {
                case false:
                    Alert.simpleAlertWithButton(message: NSLocalizedString("alert_already_exists_message_recipe", comment: ""), title: NSLocalizedString("alert_already_exists_message", comment: ""), buttonTitle: NSLocalizedString("ok_title", comment: ""), completionHandler: { (confirm) in
                        if confirm {
                            self.closeScanner()
                        }
                    })
                default:
                    Alert.simpleAlertWithButton(message: NSLocalizedString("alert_already_exists_message", comment: ""), title: NSLocalizedString("alert_already_exists_message", comment: ""), buttonTitle: NSLocalizedString("ok_title", comment: ""), completionHandler: { (confirm) in
                        if confirm {
                            self.closeScanner()
                        }
                    })
                }
            }
        }
    }
    
    func recordProduct(sku: String) {
        guard let sku = self.sku,
            let playButton = self.playButton else { return }
        self.isRecording = true
        playButton.setImage(nil, for: [])
        playButton.backgroundColor = UIColor.coopRed
        self.videoHelper.recordProduct(sku: sku, recordingDelegate: self)
        
        // set timer for recording
        self.timer = Timer.scheduledTimer(timeInterval: self.recordTime, target: self, selector: #selector(stopRecording), userInfo: nil, repeats: false)
        self.progressTimer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(updateProgressBar), userInfo: nil, repeats: true)
    }
    
    @objc func stopRecording() {
        guard let playButton = self.playButton else { return }
        playButton.setImage(UIImage(named: "play"), for: [])
        playButton.imageEdgeInsets = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
        playButton.backgroundColor = UIColor.coopOrange
        self.videoHelper.stopVideoRecording()
        self.isRecording = false
        self.timer.invalidate()
    }
    
    @objc func updateProgressBar() {
        guard let progressView = self.progressView else { return }
        
        if progressView.progress < 1 {
            progressView.progress += 0.125
        } else {
            self.progressTimer.invalidate()
        }
    }
}

extension ProductViewController: AVCaptureFileOutputRecordingDelegate {
    func fileOutput(_ output: AVCaptureFileOutput, didFinishRecordingTo outputFileURL: URL, from connections: [AVCaptureConnection], error: Error?) {
        //
        print("finished")
        Alert.progressAlert(title: NSLocalizedString("uploading_title", comment: ""), buttonTitle: NSLocalizedString("ok_title", comment: ""), completionHandler: { (confirm) in
            if confirm {
                //
                self.closeScanner()
            }
        })
        self.videoHelper.encodeVideo(videoUrl: outputFileURL, completionHandler: { (encodedURL) in
            if let savedURL = encodedURL,
                let sku = self.sku {
                print("uploading to firebase")
                self.firestorageService.uploadVideoToStorage(fileURL: savedURL, uniqueId: sku, isProduct: self.isProduct, completionHandler: { (bool) in
                    if bool {
                        print("video is uploaded")
                        self.videoHelper.deleteFile(savedURL)
                        self.firestorageService.addProductToFirestore(sku: sku)
                    } else {
                        print("error! video isn't uploaded")
                    }
                })
            }
        })
    }
    
    func fileOutput(_ output: AVCaptureFileOutput, didStartRecordingTo fileURL: URL, from connections: [AVCaptureConnection]) {
        print("started")
    }
}
