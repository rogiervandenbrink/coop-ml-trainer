//
//  CTAButton.swift
//  Coop Trainer App
//
//  Created by Rogier van den Brink on 09/05/2019.
//  Copyright © 2019 Coop. All rights reserved.
//

import UIKit

class CTAButton: UIButton {
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    init(frame: CGRect, backgroundColor: UIColor, cornerRadius: CGFloat = 3.0) {
        super.init(frame: frame)
        
        self.titleLabel?.font = UIFont.museo500Font(ofSize: 15)
        self.titleLabel?.textColor = UIColor.white
        self.backgroundColor = backgroundColor
        self.layer.cornerRadius = cornerRadius
    }
    
    override var isEnabled: Bool {
        didSet {
            if self.isEnabled {
                self.backgroundColor = UIColor.coopOrange
            } else {
                self.backgroundColor = UIColor.coopGrey
            }
        }
    }
}
