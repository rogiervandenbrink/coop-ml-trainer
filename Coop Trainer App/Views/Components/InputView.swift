//
//  InputView.swift
//  Coop Trainer App
//
//  Created by Rogier van den Brink on 17/05/2019.
//  Copyright © 2019 Coop. All rights reserved.
//

import UIKit

class InputView: UIView {
    
    @IBOutlet weak var containerView: UIView?
    @IBOutlet weak var titleLabel: UILabel?
    @IBOutlet weak var inputText: UITextField?
    @IBOutlet weak var confirmButton: CTAButton?
    
    var inputString: String = ""
    
    let userDefaults = UserDefaults.standard

    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    init(frame: CGRect, titleText: String, placeholder: String, buttonTitle: String) {
        super.init(frame: frame)
        
        let containerView = UIView(frame: CGRect(x: 13, y: (self.frame.size.height / 2) - 180, width: self.frame.size.width - 26, height: 200))
        containerView.backgroundColor = .white
        containerView.roundedCorners()
        self.addSubview(containerView)
        self.containerView = containerView
        
        let titleLabel = UILabel(frame: CGRect(x: 13, y: 13, width: self.frame.size.width - 52, height: 30))
        titleLabel.font = UIFont.museo500Font(ofSize: 18)
        titleLabel.textColor = .black
        titleLabel.text = titleText
        titleLabel.textAlignment = .center
        containerView.addSubview(titleLabel)
        self.titleLabel = titleLabel
        
        let inputText = UITextField(frame: CGRect(x: 13, y: titleLabel.frame.size.height + titleLabel.frame.origin.y + 13, width: self.frame.size.width - 52, height: 44))
        inputText.keyboardType = .emailAddress
        inputText.placeholder = placeholder
        inputText.addDoneButtonOnKeyboard()
        containerView.addSubview(inputText)
        self.inputText = inputText
        
        let bottomLine = CALayer()
        bottomLine.frame = CGRect(x: 0.0, y: inputText.frame.height - 1, width: inputText.frame.width, height: 1.0)
        bottomLine.backgroundColor = UIColor.black.cgColor
        inputText.borderStyle = .none
        inputText.layer.addSublayer(bottomLine)
        
        let confirmButton = CTAButton(frame: CGRect(x: 13, y: inputText.frame.size.height + inputText.frame.origin.y + 25, width: self.frame.size.width - 52, height: 44), backgroundColor: UIColor.coopOrange)
        confirmButton.setTitle(buttonTitle, for: [])
        confirmButton.isEnabled = false
        containerView.addSubview(confirmButton)
        self.confirmButton = confirmButton
        
        inputText.reactive.text.observeNext { [unowned self] text in
            if let input = text {
                print("inputField: \(input)")
                self.inputString = input
                if self.isValidEmail(testStr: input) {
                    guard let confirmButton = self.confirmButton else { return }
                    confirmButton.isEnabled = true
                }
            }
        }.dispose(in: reactive.bag)
        
        confirmButton.reactive.tap.observeNext { [unowned self] (_) in
            self.saveEmail()
        }.dispose(in: reactive.bag)
    }
    
    func saveEmail() {
        if !self.inputString.isEmpty {
            self.userDefaults.set(self.inputString, forKey: Constants.emailAdress)
        } else {
            Alert.simpleAlert(message: "Er gaat iets mis, probeer het later nogmaals.")
        }
    }
    
    func isValidEmail(testStr: String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
}
