//
//  RecipeViewController.swift
//  Coop Trainer App
//
//  Created by Rogier van den Brink on 10/05/2019.
//  Copyright © 2019 Coop. All rights reserved.
//

import UIKit
import Bond

class RecipeViewController: UIViewController {

    private var productViewController: UIViewController?
    
    @IBOutlet weak var containerView: UIView?
    @IBOutlet weak var titleLabel: UILabel?
    @IBOutlet weak var inputText: UITextField?
    @IBOutlet weak var confirmButton: CTAButton?
    
    var recipeId: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.view.backgroundColor = UIColor.coopBackground
        
        setup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        guard let inputText = self.inputText else { return }
        
        inputText.text = ""
    }
    
    func setup() {
        let containerView = UIView(frame: CGRect(x: 13, y: (self.view.frame.size.height / 2) - 180, width: self.view.frame.size.width - 26, height: 200))
        containerView.backgroundColor = .white
        containerView.roundedCorners()
        containerView.dropShadow()
        self.view.addSubview(containerView)
        self.containerView = containerView
        
        let titleLabel = UILabel(frame: CGRect(x: 13, y: 13, width: self.view.frame.size.width - 52, height: 30))
        titleLabel.font = UIFont.museo500Font(ofSize: 18)
        titleLabel.textColor = .black
        titleLabel.text = NSLocalizedString("recipe_instruction_title", comment: "")
        titleLabel.textAlignment = .center
        containerView.addSubview(titleLabel)
        self.titleLabel = titleLabel
        
        let inputText = UITextField(frame: CGRect(x: 13, y: titleLabel.frame.size.height + titleLabel.frame.origin.y + 13, width: self.view.frame.size.width - 52, height: 44))
        inputText.keyboardType = .phonePad
        inputText.placeholder = NSLocalizedString("recipeId_placeholder", comment: "")
        inputText.addDoneButtonOnKeyboard()
        containerView.addSubview(inputText)
        self.inputText = inputText
        
        let bottomLine = CALayer()
        bottomLine.frame = CGRect(x: 0.0, y: inputText.frame.height - 1, width: inputText.frame.width, height: 1.0)
        bottomLine.backgroundColor = UIColor.black.cgColor
        inputText.borderStyle = .none
        inputText.layer.addSublayer(bottomLine)
        
        let confirmButton = CTAButton(frame: CGRect(x: 13, y: inputText.frame.size.height + inputText.frame.origin.y + 25, width: self.view.frame.size.width - 52, height: 44), backgroundColor: UIColor.coopOrange)
        confirmButton.setTitle(NSLocalizedString("recipe_button_title", comment: ""), for: [])
        containerView.addSubview(confirmButton)
        self.confirmButton = confirmButton
        
        inputText.reactive.text.observeNext { [unowned self] text in
            if let input = text {
                self.recipeId = input
                print("recipeId: \(input)")
            }
        }.dispose(in: reactive.bag)
        
        confirmButton.reactive.tap.observeNext { [unowned self] (_) in
            if !self.recipeId.isEmpty {
                self.openProductScan(recipeId: self.recipeId)
            }
        }.dispose(in: reactive.bag)
    }
    
    func openProductScan(recipeId: String) {
        let viewController = ProductViewController()
        
        let closeView = StyleKit.closeIconView(frame: CGRect(x: 0, y: 0, width: 17, height: 17), color: UIColor.white)
        let renderer = UIGraphicsImageRenderer(size: closeView.bounds.size)
        let image = renderer.image { ctx in
            closeView.drawHierarchy(in: closeView.bounds, afterScreenUpdates: true)
        }
        let closeIconImage = image
        
        let done: UIBarButtonItem = UIBarButtonItem(image: closeIconImage, style: .done, target: self, action: #selector(self.hideScanner))
        viewController.navigationItem.setLeftBarButton(done, animated: false)
        
        let navController = UINavigationController(rootViewController: viewController)
        navController.view.backgroundColor = UIColor.white
        
        self.productViewController = navController
        
        self.present(navController, animated: true, completion: {
            //
            viewController.isProduct = false
            viewController.sku = recipeId
            
        })
    }
    
    @objc func hideScanner() {
        if let productViewController = self.productViewController {
            productViewController.dismiss(animated: true, completion: {
                //
            })
        }
    }

}
