//
//  ViewController.swift
//  Coop Trainer App
//
//  Created by Rogier van den Brink on 08/05/2019.
//  Copyright © 2019 Coop. All rights reserved.
//

import UIKit
import AVFoundation
import Firebase

class ViewController: UIViewController {

    @IBOutlet var tabsView: TabsView?
    @IBOutlet weak var containerView: UIView?
    @IBOutlet var previewView: UIImageView?
    @IBOutlet var instructionLabel: UILabel?
    @IBOutlet var scoreLabel: UILabel?
    var session: AVCaptureSession?
    
    let userDefaults = UserDefaults.standard
    
    var firestorageService = FirestorageService()
    
    var openedScanner: Bool = false
    
    private var productViewController: UIViewController?
    
    override func viewDidLoad() {
        setup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.updateAmount()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.checkIfNeedsEmail()
    }
    
    private func setup() {
        //containerview
        let containerView = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height - 160))
        containerView.backgroundColor = UIColor.coopBackground
        self.containerView = containerView
        self.view.addSubview(containerView)
        
        let bottomView = UIView(frame: CGRect(x: 0, y: self.view.frame.size.height - 220, width: self.view.frame.size.width, height: 220))
        bottomView.backgroundColor = .white
        self.view.addSubview(bottomView)
        
        let instructionLabel = UILabel(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 60))
        instructionLabel.textAlignment = .center
        instructionLabel.textColor = .black
        instructionLabel.font = UIFont.museo500Font(ofSize: 15)
        instructionLabel.text = NSLocalizedString("instruction_title", comment: "")
        bottomView.addSubview(instructionLabel)
        self.instructionLabel = instructionLabel
        
        let tabsView = TabsView(frame: CGRect(x: 0, y: 60, width: self.view.frame.size.width, height: 70))
        tabsView.tabTitles = ["Product", "Recept"]
        tabsView.dropShadow(height: -4.0)
        bottomView.addSubview(tabsView)
        self.tabsView = tabsView
        
        tabsView.selectedTab.observeNext { [unowned self] (_) in
            self.updateContainerView()
        }.dispose(in: reactive.bag)
        
        let scoreLabel = UILabel(frame: CGRect(x: 0, y: tabsView.frame.size.height + tabsView.frame.origin.y + 5, width: self.view.frame.size.width, height: 60))
        scoreLabel.textAlignment = .center
        scoreLabel.textColor = .black
        scoreLabel.font = UIFont.museo500Font(ofSize: 17)
        scoreLabel.adjustsFontSizeToFitWidth = true
        scoreLabel.text = NSLocalizedString("score_title_label", comment: "")
        bottomView.addSubview(scoreLabel)
        self.scoreLabel = scoreLabel
        
        self.firestorageService.checkAmount { (amount) in
            self.updateLabel(amount: amount)
        }
    }
    
    //switch between productinfo and recipes
    private lazy var barcodeViewController: BarcodeViewController = {
        let viewController = BarcodeViewController()
        self.add(asChildViewController: viewController)
        
        return viewController
    }()
    
    private lazy var recipeViewController: RecipeViewController = {
        let viewController = RecipeViewController()
        self.add(asChildViewController: viewController)
        
        return viewController
    }()
    
    private func add(asChildViewController viewController: UIViewController) {
        addChild(viewController)
        if let containerView = self.containerView {
            containerView.addSubview(viewController.view)
            
            // Configure Child View
            viewController.view.frame = CGRect(x: 0, y: 0, width: containerView.bounds.size.width, height: containerView.bounds.size.height)
        }
        
        viewController.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        // Notify Child View Controller
        viewController.didMove(toParent: self)
    }
    
    private func remove(asChildViewController viewController: UIViewController) {
        // Notify Child View Controller
        viewController.willMove(toParent: nil)
        // Remove Child View From Superview
        viewController.view.removeFromSuperview()
        // Notify Child View Controller
        viewController.removeFromParent()
        viewController.dismiss(animated: false) {
            //
        }
    }
    
    private func updateContainerView() {
        
        if let tabView = self.tabsView,
            let instructionLabel = self.instructionLabel {
            let tabIndex = tabView.selectedTab.value
            switch tabIndex {
            case 0:
                // Product Scanner
                instructionLabel.text = NSLocalizedString("instruction_title", comment: "")
                remove(asChildViewController: recipeViewController)
                add(asChildViewController: barcodeViewController)
            case 1:
                // Recipe Scanner
                instructionLabel.text = ""
                remove(asChildViewController: barcodeViewController)
                add(asChildViewController: recipeViewController)
            default:
                // Product Scanner
                instructionLabel.text = NSLocalizedString("instruction_title", comment: "")
                remove(asChildViewController: recipeViewController)
                add(asChildViewController: barcodeViewController)
            }
        }
    }
    
    func openProductScan(sku: String) {
        let viewController = ProductViewController()
        
        let closeView = StyleKit.closeIconView(frame: CGRect(x: 0, y: 0, width: 17, height: 17), color: UIColor.white)
        let renderer = UIGraphicsImageRenderer(size: closeView.bounds.size)
        let image = renderer.image { ctx in
            closeView.drawHierarchy(in: closeView.bounds, afterScreenUpdates: true)
        }
        let closeIconImage = image
        
        let done: UIBarButtonItem = UIBarButtonItem(image: closeIconImage, style: .done, target: self, action: #selector(self.hideScanner))
        viewController.navigationItem.setLeftBarButton(done, animated: false)
        
        let navController = UINavigationController(rootViewController: viewController)
        navController.view.backgroundColor = UIColor.white
        
        self.productViewController = navController
        
        self.present(navController, animated: true, completion: {
            //
            viewController.sku = sku
        })
    }
    
    @objc func hideScanner() {
        if let productViewController = self.productViewController {
            productViewController.dismiss(animated: true, completion: {
                //
            })
        }
    }
    
    func checkIfNeedsEmail() {
        if let emailAdres = self.userDefaults.string(forKey: Constants.emailAdress),
            !emailAdres.isEmpty {
            
        } else {
            // show alert for emailadress
            Alert.inputAlert(title: NSLocalizedString("input_email_title", comment: ""), placeholder: NSLocalizedString("input_email_placeholder", comment: "")) { (ready) in
                if ready {
                    //
                    self.updateAmount()
                }
            }
        }
    }
    
    func updateAmount() {
        self.firestorageService.countFilmedProducts { (amount) in
            self.updateLabel(amount: amount)
        }
    }
    
    func updateLabel(amount: Int) {
        guard let scoreLabel = self.scoreLabel else { return }
        if amount == 1 {
            scoreLabel.text = String(format: NSLocalizedString("score_title_label_single", comment: ""), "\(amount)")
        } else {
            scoreLabel.text = String(format: NSLocalizedString("score_title_label", comment: ""), "\(amount)")
        }
        self.firestorageService.addAmountFields(amount: amount)
    }
}
