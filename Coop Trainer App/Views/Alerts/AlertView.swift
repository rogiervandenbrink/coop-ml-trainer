//
//  AlertView.swift
//  Coop Trainer App
//
//  Created by Rogier van den Brink on 09/05/2019.
//  Copyright © 2019 Coop. All rights reserved.
//

import UIKit
import Bond

class AlertView: UIView {
    
    var iconHolder: UIImageView?
    var categoryTitleLabel: UILabel?
    var titleLabel: UILabel?
    var messageLabel: UILabel?
    var closeButton: UIButton?
    var actionButton: CTAButton?
    var cancelButton: CTAButton?
    var highlightedButton: UIButton?
    
    var windowBackground: UIView?
    var backgroundView: UIView?
    
    // Animation constants
    let backgroundStartAlpha: CGFloat = 0.0
    let backgroundEndAlpha: CGFloat = 0.75
    let windowStartScale: CGFloat = 0.8
    let windowEndScale: CGFloat = 1.0
    let subitemStartAlpha: CGFloat = 0.0
    let subitemEndAlpha: CGFloat = 1.0
    
    var title: String = ""
    var categoryTitle: String = ""
    var message: String = ""
    var buttonTitle: String = ""
    var cancelButtonTitle: String = ""
    
    var twoActionButtons: Bool = false
    var hideIcon: Bool = false
    var showHighLight: Bool = false
    var showCloseButton: Bool = false
    var iconImageName: String = "telephoneBlack"
    
    var linkInMessage: String?
    
    // Algemene voorwaarden
    var linkInMessageClicked = Observable(false)
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    init(frame: CGRect, title: String = "", categoryTitle: String = "", message: String = "", buttonTitle: String = "", cancelButtonTitle: String = "", twoActionButtons: Bool = false, showCloseButton: Bool = false, hideIcon: Bool = false, showHighLight: Bool = false, iconImageName: String = "telephoneBlack") {
        super.init(frame: frame)
        
        self.categoryTitle = categoryTitle
        self.title = title
        self.message = message
        self.buttonTitle = buttonTitle
        self.cancelButtonTitle = cancelButtonTitle
        self.twoActionButtons = twoActionButtons
        self.hideIcon = hideIcon
        self.showHighLight = showHighLight
        self.showCloseButton = showCloseButton
        self.iconImageName = iconImageName
        
        setup()
    }
    
    fileprivate func setup() {
        
        let backgroundView = UIView(frame: self.bounds)
        backgroundView.backgroundColor = UIColor.coopDeepBlack
        backgroundView.alpha = backgroundStartAlpha
        self.addSubview(backgroundView)
        self.backgroundView = backgroundView
        
        // Check if we want to show a highlighted item
        if self.showHighLight {
            let radius: CGFloat = 20.0
            
            let maskLayer = CAShapeLayer()
            maskLayer.fillColor = UIColor.black.cgColor
            maskLayer.backgroundColor = UIColor.black.cgColor
            
            let outerPath = UIBezierPath(rect: self.bounds)
            
            var topOffset: CGFloat = 22.0
            if UIScreen.current == .iPhone5_8 {
                //if iphone x lower highlighting
                topOffset = 47.0
            }
            
            let path = UIBezierPath(ovalIn: CGRect(x: self.bounds.size.width - 51, y: topOffset, width: radius * 2.0, height: radius * 2.0))
            outerPath.usesEvenOddFillRule = true
            outerPath.append(path)
            
            let shapeLayer = CAShapeLayer()
            shapeLayer.path = outerPath.cgPath
            shapeLayer.fillColor = UIColor.white.cgColor
            shapeLayer.fillRule = CAShapeLayerFillRule.evenOdd
            maskLayer.addSublayer(shapeLayer)
            
            backgroundView.layer.mask = maskLayer
            
            // Highlightedbutton
            let highlightedButton = UIButton(frame: CGRect(x: self.bounds.size.width - 52, y: topOffset - 2, width: radius * 2.0 + 1, height: radius * 2.0 + 1))
            highlightedButton.backgroundColor = UIColor.clear
            self.addSubview(highlightedButton)
            self.highlightedButton = highlightedButton
        }
        
        let windowBackground = UIView(frame: CGRect(x: 28, y: self.frame.size.height/2 - 110, width: self.frame.size.width - 56, height: 220))
        windowBackground.backgroundColor = UIColor.white
        windowBackground.layer.cornerRadius = 3.0
        windowBackground.layer.shadowColor = UIColor.black.cgColor
        windowBackground.layer.shadowOpacity = 0.2
        windowBackground.layer.shadowRadius = 4.0
        windowBackground.layer.shadowOffset = CGSize(width: 0, height: 2)
        self.addSubview(windowBackground)
        windowBackground.alpha = 0.0
        self.windowBackground = windowBackground
        
        let iconHolder = UIImageView(frame: CGRect(x: windowBackground.frame.size.width/2 - 9, y: 19, width: 18, height: 30))
        iconHolder.contentMode = .scaleAspectFit
        iconHolder.image = UIImage(named: iconImageName)
        windowBackground.addSubview(iconHolder)
        iconHolder.alpha = 0.0
        iconHolder.isHidden = hideIcon
        self.iconHolder = iconHolder
        
        let categoryTitleLabel = UILabel(frame: CGRect(x: 23, y: 21, width: windowBackground.frame.size.width - 46, height: 24))
        categoryTitleLabel.font = UIFont.museo700Font(ofSize: 18)
        categoryTitleLabel.textAlignment = .center
        categoryTitleLabel.textColor = UIColor.coopOrange
        categoryTitleLabel.text = self.categoryTitle
        categoryTitleLabel.alpha = 0.0
        windowBackground.addSubview(categoryTitleLabel)
        self.categoryTitleLabel = categoryTitleLabel
        
        let titleY: CGFloat = (self.hideIcon && self.categoryTitle.isEmpty) ? 19 : 62
        let titleLabel = UILabel(frame: CGRect(x: 23, y: titleY, width: windowBackground.frame.size.width - 46.0, height: 24))
        titleLabel.font = UIFont.museo700Font(ofSize: 18)
        titleLabel.textAlignment = .center
        titleLabel.textColor = UIColor.black
        titleLabel.numberOfLines = 0
        titleLabel.text = self.title
        titleLabel.numberOfLines = 0
        titleLabel.lineBreakMode = .byWordWrapping
        
        windowBackground.addSubview(titleLabel)
        titleLabel.alpha = 0.0
        self.titleLabel = titleLabel
        
        titleLabel.snp.makeConstraints { (make) in
            make.width.equalTo(windowBackground).offset(-46)
            make.top.equalTo(windowBackground).offset(titleY)
            make.left.equalTo(windowBackground).offset(23)
            make.right.equalTo(windowBackground).offset(-23)
        }
        titleLabel.sizeToFit()
        
        let messageLabelY = self.title.isEmpty ? titleY : titleY + titleLabel.frame.size.height + 4
        let messageLabel = UILabel(frame: CGRect(x: 23.0, y: Double(messageLabelY), width: Double(windowBackground.frame.size.width - 46.0), height: 50.0))
        messageLabel.font = UIFont.museo300Font(ofSize: 15)
        messageLabel.numberOfLines = 0
        messageLabel.textAlignment = .center
        messageLabel.textColor = UIColor.black
        windowBackground.addSubview(messageLabel)
        
        let attributedString = NSMutableAttributedString(string: self.message, attributes: [NSAttributedString.Key.foregroundColor: UIColor.black, NSAttributedString.Key.font: UIFont.museo300Font(ofSize: 15)])
        
        messageLabel.attributedText = attributedString
        messageLabel.alpha = 0.0
        messageLabel.sizeToFit()
        messageLabel.frame = CGRect(x: CGFloat(23.0), y: CGFloat(messageLabelY), width: windowBackground.frame.size.width - 46.0, height: CGFloat(messageLabel.frame.size.height))
        self.messageLabel = messageLabel
        
        setupButtons()
    }
    
    func setupButtons() {
        if let windowBackground = self.windowBackground,
            let messageLabel = self.messageLabel {
            let actionButton = CTAButton(frame: CGRect(x: 23, y: messageLabel.frame.origin.y + messageLabel.frame.size.height + 20, width: windowBackground.bounds.size.width - 46, height: 44), backgroundColor: UIColor.coopOrange)
            actionButton.setTitle(self.buttonTitle, for: .normal)
            windowBackground.addSubview(actionButton)
            actionButton.alpha = 0.0
            self.actionButton = actionButton
            
            let cancelButton = CTAButton(frame: CGRect(x: (windowBackground.bounds.size.width / 2) + 23, y: messageLabel.frame.origin.y + messageLabel.frame.size.height + 20, width: (windowBackground.bounds.size.width / 2) - 46, height: 44), backgroundColor: UIColor.coopOrange)
            cancelButton.setTitle(self.cancelButtonTitle, for: .normal)
            windowBackground.addSubview(cancelButton)
            cancelButton.alpha = 0.0
            cancelButton.isEnabled = false
            self.cancelButton = cancelButton
            
            let closeButton = UIButton(frame: CGRect(x: windowBackground.frame.size.width - 25, y: 19, width: 15, height: 15))
            closeButton.backgroundColor = UIColor.white
            closeButton.imageView?.contentMode = .center
            closeButton.setImage(UIImage(named: "closeIcon"), for: .normal)
            closeButton.isHidden = !showCloseButton
            windowBackground.addSubview(closeButton)
            self.closeButton = closeButton
            
            // We do this as last, otherwise positioning is off the grid.
            let newHeight = actionButton.frame.origin.y + 44 + 23
            windowBackground.frame = CGRect(x: 28, y: self.frame.size.height/2 - newHeight/2, width: self.frame.size.width - 56, height: newHeight)
            windowBackground.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
        }
    }
    
    // Present the alert
    func present(completionHandler: @escaping (Bool) -> Void) {
        
        if let actionButton = self.actionButton {
            actionButton.reactive.tap.observeNext { [unowned self] in
                completionHandler(true)
                
                self.hide()
                }.dispose(in: self.reactive.bag)
        }
        
        if let cancelButton = self.cancelButton {
            cancelButton.reactive.tap.observeNext { [unowned self] in
                completionHandler(false)
                
                self.hide()
                }.dispose(in: self.reactive.bag)
        }
        
        if let closeButton = self.closeButton {
            closeButton.reactive.tap.observeNext { [unowned self] in
                completionHandler(false)
                
                self.hide()
                }.dispose(in: self.reactive.bag)
        }
        
        if let backgroundView = self.backgroundView {
            backgroundView.reactive.tapGesture().observeNext(with: { [unowned self] (_) in
                completionHandler(false)
                self.hide()
            }).dispose(in: self.reactive.bag)
        }
        
        // Startanimation
        if let backgroundView = self.backgroundView,
            let windowBackground = self.windowBackground,
            let iconHolder = self.iconHolder,
            let titleLabel = self.titleLabel,
            let categoryTitleLabel = self.categoryTitleLabel,
            let messageLabel = self.messageLabel,
            let actionButton = self.actionButton,
            let cancelButton = self.cancelButton {
            
            UIView.animate(withDuration: 0.2, delay: 0.1, options: .curveEaseOut, animations: {
                backgroundView.alpha = self.backgroundEndAlpha
            }, completion: { (_) in
                //
            })
            
            UIView.animate(withDuration: 0.3, delay: 0.2, usingSpringWithDamping: 1.1, initialSpringVelocity: 0.7, options: .curveEaseOut, animations: {
                windowBackground.alpha = 1.0
                windowBackground.transform = CGAffineTransform(scaleX: self.windowEndScale, y: self.windowEndScale)
            }, completion: { (_) in
                UIView.animate(withDuration: 0.15, delay: 0.0, options: .curveEaseOut, animations: {
                    
                    if self.categoryTitle.isEmpty {
                        iconHolder.alpha = 1.0
                    }
                    
                }, completion: { (_) in
                    //
                })
                UIView.animate(withDuration: 0.1, delay: 0.05, options: .curveEaseOut, animations: {
                    titleLabel.alpha = 1.0
                }, completion: { (_) in
                    //
                })
                UIView.animate(withDuration: 0.1, delay: 0.05, options: .curveEaseOut, animations: {
                    if !self.categoryTitle.isEmpty {
                        categoryTitleLabel.alpha = 1.0
                    }
                }, completion: { (_) in
                    //
                })
                UIView.animate(withDuration: 0.1, delay: 0.1, options: .curveEaseOut, animations: {
                    messageLabel.alpha = 1.0
                }, completion: { (_) in
                    //
                })
                UIView.animate(withDuration: 0.1, delay: 0.15, options: .curveEaseOut, animations: {
                    actionButton.alpha = 1.0
                    if self.twoActionButtons {
                        cancelButton.alpha = 1.0
                        
                    } else {
                        cancelButton.alpha = 0.0
                    }
                }, completion: { (_) in
                    //
                })
                
                cancelButton.isEnabled = self.twoActionButtons
                // width button
                if self.twoActionButtons {
                    actionButton.frame.size.width = (windowBackground.bounds.size.width / 2) - 46
                } else {
                    actionButton.frame.size.width = windowBackground.bounds.size.width - 46
                }
            })
        }
    }
    
    func hide() {
        // Let alertView know something
        if self.showCloseButton {
            // Make sure a cancel action will be called
        }
        
        if let backgroundView = self.backgroundView,
            let windowBackground = self.windowBackground,
            let iconHolder = self.iconHolder,
            let titleLabel = self.titleLabel,
            let categoryTitleLabel = self.categoryTitleLabel,
            let messageLabel = self.messageLabel,
            let actionButton = self.actionButton,
            let cancelButton = self.cancelButton {
            
            UIView.animate(withDuration: 0.1, delay: 0.1, options: .curveEaseOut, animations: {
                actionButton.alpha = 0.0
                cancelButton.alpha = 0.0
            }, completion: { (_) in
                //
            })
            UIView.animate(withDuration: 0.1, delay: 0.15, options: .curveEaseOut, animations: {
                messageLabel.alpha = 0.0
            }, completion: { (_) in
                //
            })
            UIView.animate(withDuration: 0.1, delay: 0.2, options: .curveEaseOut, animations: {
                titleLabel.alpha = 0.0
            }, completion: { (_) in
                //
            })
            UIView.animate(withDuration: 0.1, delay: 0.2, options: .curveEaseOut, animations: {
                categoryTitleLabel.alpha = 0.0
            }, completion: { (_) in
                //
            })
            UIView.animate(withDuration: 0.1, delay: 0.25, options: .curveEaseOut, animations: {
                iconHolder.alpha = 0.0
            }, completion: { (_) in
                //
            })
            
            UIView.animate(withDuration: 0.25, delay: 0.3, usingSpringWithDamping: 1.1, initialSpringVelocity: 0.8, options: .curveEaseOut, animations: {
                windowBackground.alpha = 0.0
                windowBackground.transform = CGAffineTransform(scaleX: self.windowStartScale, y: self.windowStartScale)
            }, completion: { (_) in
                UIView.animate(withDuration: 0.15, delay: 0.0, options: .curveEaseOut, animations: {
                    backgroundView.alpha = 0.0
                }, completion: { (_) in
                    //
                    self.removeFromSuperview()
                })
            })
            cancelButton.isEnabled = false
        }
    }
}
