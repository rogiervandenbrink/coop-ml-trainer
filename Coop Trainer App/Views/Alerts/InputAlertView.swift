//
//  InputAlertView.swift
//  Coop Trainer App
//
//  Created by Rogier van den Brink on 17/05/2019.
//  Copyright © 2019 Coop. All rights reserved.
//

import UIKit

class InputAlertView: UIView {

    // Animation constants
    let backgroundStartAlpha: CGFloat = 0.0
    let backgroundEndAlpha: CGFloat = 0.75
    let windowStartScale: CGFloat = 0.8
    let windowEndScale: CGFloat = 1.0
    let subitemStartAlpha: CGFloat = 0.0
    let subitemEndAlpha: CGFloat = 1.0
    
    // layouts
    var backgroundView: UIView?
    var windowView: InputView?
    
    var title: String = ""
    var placeHolder: String = ""
    var buttonTitle: String = ""
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    init(frame: CGRect, title: String = "", placeHolder: String = "", buttonTitle: String = "") {
        super.init(frame: frame)
        
        self.title = title
        self.placeHolder = placeHolder
        self.buttonTitle = buttonTitle
        
        setup()
    }
    
    fileprivate func setup() {
        
        let backgroundView = UIView(frame: self.bounds)
        backgroundView.backgroundColor = UIColor.coopDeepBlack
        backgroundView.alpha = backgroundStartAlpha
        self.addSubview(backgroundView)
        self.backgroundView = backgroundView
        
        let windowView = InputView(frame: CGRect(x: 13, y: (self.frame.size.height / 2) - 180, width: self.frame.size.width - 26, height: 200), titleText: self.title, placeholder: self.placeHolder, buttonTitle: self.buttonTitle)
        windowView.alpha = 0.0
        self.addSubview(windowView)
        self.windowView = windowView
    }
    
    // Present the alert
    func present(completionHandler: @escaping (Bool) -> Void) {
        
        if let windowView = self.windowView,
            let actionButton = windowView.confirmButton {
            actionButton.reactive.tap.observeNext { [unowned self] in
                completionHandler(true)
                
                self.hide()
            }.dispose(in: self.reactive.bag)
        }
        
        if let backgroundView = self.backgroundView {
            backgroundView.reactive.tapGesture().observeNext(with: { [unowned self] (_) in
                completionHandler(false)
                //self.hide()
            }).dispose(in: self.reactive.bag)
        }
        
        // Startanimation
        if let backgroundView = self.backgroundView,
            let windowView = self.windowView {
            
            UIView.animate(withDuration: 0.2, delay: 0.1, options: .curveEaseOut, animations: {
                backgroundView.alpha = self.backgroundEndAlpha
            }, completion: { (_) in
                //
            })
            
            
            UIView.animate(withDuration: 0.3, delay: 0.2, usingSpringWithDamping: 1.1, initialSpringVelocity: 0.7, options: .curveEaseOut, animations: {
                windowView.alpha = 1.0
                windowView.transform = CGAffineTransform(scaleX: self.windowEndScale, y: self.windowEndScale)
            }, completion: { (_) in
                
            })
        }
    }
    
    func hide() {
        
        if let backgroundView = self.backgroundView,
            let windowView = self.windowView {
            
            UIView.animate(withDuration: 0.25, delay: 0.3, usingSpringWithDamping: 1.1, initialSpringVelocity: 0.8, options: .curveEaseOut, animations: {
                windowView.alpha = 0.0
                windowView.transform = CGAffineTransform(scaleX: self.windowStartScale, y: self.windowStartScale)
            }, completion: { (_) in
                UIView.animate(withDuration: 0.15, delay: 0.0, options: .curveEaseOut, animations: {
                    backgroundView.alpha = 0.0
                }, completion: { (_) in
                    //
                    self.removeFromSuperview()
                })
            })
        }
    }
}
