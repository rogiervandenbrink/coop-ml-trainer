//
//  TabsCollectionView.swift
//  Coop Trainer App
//
//  Created by Rogier van den Brink on 08/05/2019.
//  Copyright © 2019 Coop. All rights reserved.
//

import UIKit
import Bond

class TabsCollectionView: UICollectionView, UICollectionViewDelegate, UICollectionViewDataSource {
    
    fileprivate let reuseIdentifier = "TabCollectionViewCell"
    public var tabTitles = [String]()
    public var selectedItem = Observable(0)
    public var selectedX = Observable(0.0)
    public var selectedCellWidth = Observable(UIScreen.main.bounds.width / 2)
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return tabTitles.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as? TabCollectionViewCell else {
            fatalError("Could not dequeue cell with identifier: TabCollectionViewCell")
        }
        
        cell.tabTitles = tabTitles
        cell.title = tabTitles[indexPath.row]
        
        if indexPath.row == selectedItem.value {
            cell.isSelected = true
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        for cell in collectionView.visibleCells {
            cell.isSelected = false
        }
        
        collectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
        
        //for animation selectedborder
        self.getSizeForItem(collectionView: collectionView, indexPath: indexPath)
        
        self.selectedItem.value = indexPath.row
    }
    
    func getSizeForItem(collectionView: UICollectionView, indexPath: IndexPath) {
        
        if let attributes = collectionView.layoutAttributesForItem(at: indexPath) {
            let rect = attributes.frame
            let cellWidth = rect.size.width
            
            self.selectedX.value = Double(rect.origin.x)
            self.selectedCellWidth.value = cellWidth
        }
    }
}
