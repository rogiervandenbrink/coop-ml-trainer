//
//  TabsView.swift
//  Coop Trainer App
//
//  Created by Rogier van den Brink on 08/05/2019.
//  Copyright © 2019 Coop. All rights reserved.
//

import UIKit
import Bond

class TabsView: UIView {
    
    @IBOutlet weak var collectionView: TabsCollectionView?
    @IBOutlet weak var selectedBorderView: SelectedBorderView?
    
    let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
    let itemWidth: CGFloat = UIScreen.main.bounds.width / 2
    
    public var selectedTab = Observable(0)
    
    public var tabTitles: [String]? {
        didSet {
            if let tabTitles = self.tabTitles, !tabTitles.isEmpty {
                updateProperties()
            }
        }
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setup()
    }
    
    fileprivate func setup() {
        self.backgroundColor = UIColor.white
        
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        layout.estimatedItemSize = CGSize(width: itemWidth, height: self.frame.size.height - 1)
        layout.scrollDirection = .horizontal
        
        let collectionView = TabsCollectionView(frame: CGRect(x: 0, y: 0, width: self.frame.size.width, height: self.frame.size.height - 1), collectionViewLayout: layout)
        collectionView.register(TabCollectionViewCell.self, forCellWithReuseIdentifier: "TabCollectionViewCell")
        collectionView.delegate = collectionView
        collectionView.dataSource = collectionView
        collectionView.collectionViewLayout = layout
        collectionView.backgroundColor = UIColor.white
        collectionView.showsHorizontalScrollIndicator = false
        self.addSubview(collectionView)
        self.collectionView = collectionView
        
        //get selected tab
        collectionView.selectedItem.observeNext { [unowned self] (tab) in
            
            self.selectedTab.value = tab
            
            let width = CGFloat(collectionView.selectedCellWidth.value)
            let xPosition = CGFloat(collectionView.selectedX.value)
            self.animateSelectedBorder(xPosition: xPosition, width: width)
            
            }.dispose(in: reactive.bag)
        
        let selectedBorderView = SelectedBorderView(frame: CGRect(x: 0, y: self.frame.size.height - 5, width: itemWidth, height: 4))
        collectionView.addSubview(selectedBorderView)
        self.selectedBorderView = selectedBorderView
        
        let startPoint = CGPoint(x: 0, y: self.frame.size.height - 1)
        let endPoint = CGPoint(x: self.frame.size.width, y: self.frame.size.height - 1)
        self.drawLine(fromPoint: startPoint, toPoint: endPoint)
    }
    
    func drawLine(fromPoint start: CGPoint, toPoint end: CGPoint) {
        let line = CAShapeLayer()
        let linePath = UIBezierPath()
        linePath.move(to: start)
        linePath.addLine(to: end)
        line.path = linePath.cgPath
        line.strokeColor = UIColor.coopBorderGrey.cgColor
        line.lineWidth = 1
        line.lineJoin = CAShapeLayerLineJoin.round
        self.layer.addSublayer(line)
    }
    
    func updateProperties() {
        if let collectionView = self.collectionView,
            let tabTitles = self.tabTitles {
            
            collectionView.tabTitles = tabTitles
            collectionView.reloadData()
        }
    }
    
    func animateSelectedBorder(xPosition: CGFloat, width: CGFloat) {
        if let selectedBorderView = self.selectedBorderView,
            let tabTitles = self.tabTitles {
            
            UIView.animate(withDuration: 0.3) {
                
                if tabTitles.count == 1 {
                    selectedBorderView.frame = CGRect(x: 0, y: self.frame.size.height - 5, width: self.frame.size.width, height: 4)
                } else {
                    selectedBorderView.frame = CGRect(x: xPosition, y: self.frame.size.height - 5, width: width, height: 4)
                }
            }
        }
    }
}
