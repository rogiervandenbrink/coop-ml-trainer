//
//  TabCollectionViewCell.swift
//  Coop Trainer App
//
//  Created by Rogier van den Brink on 08/05/2019.
//  Copyright © 2019 Coop. All rights reserved.
//

import UIKit
import SnapKit

class TabCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var titleLabel: UILabel?
    
    var title: String? {
        didSet {
            updateProperties()
        }
    }
    
    var tabTitles: [String]?
    
    override var isSelected: Bool {
        didSet {
            selectedCell(selected: self.isSelected)
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        // Initialization code
        
        self.backgroundColor = UIColor.transparentWhite
        self.layer.cornerRadius = self.frame.size.height / 2
        self.layer.masksToBounds = true
        
        let titleLabel = UILabel(frame: CGRect(x: 13, y: 15, width: self.frame.size.width - 26, height: 30))
        titleLabel.text = " "
        titleLabel.textAlignment = .center
        titleLabel.textColor = UIColor.coopQuantityGrey
        titleLabel.font = UIFont.museo500Font(ofSize: 18)
        titleLabel.lineBreakMode = .byClipping
        self.contentView.addSubview(titleLabel)
        self.titleLabel = titleLabel
        
        self.setConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func updateProperties() {
        if let titleLabel = self.titleLabel, let title = self.title {
            
            titleLabel.text = title
            titleLabel.sizeToFit()
            titleLabel.frame = CGRect(x: 13, y: titleLabel.frame.origin.y, width: titleLabel.frame.size.width, height: 30)
            
            self.setConstraints()
        }
    }
    
    func setConstraints() {
        if let titleLabel = self.titleLabel,
            let tabTitles = self.tabTitles {
            
            let screenWidth = UIScreen.main.bounds.width
            let margin = (self.frame.size.height - 30) / 2
            
            if tabTitles.count == 1 {
                titleLabel.snp.remakeConstraints { (make) in
                    make.bottom.equalTo(self.contentView).offset(-margin)
                    make.left.equalTo(self.contentView).offset(13)
                    make.width.equalTo(screenWidth)
                    make.height.equalTo(30)
                    make.right.equalTo(self.contentView).offset(-13)
                }
            } else if tabTitles.count == 2 {
                titleLabel.snp.remakeConstraints { (make) in
                    make.bottom.equalTo(self.contentView).offset(-margin)
                    make.left.equalTo(self.contentView).offset(13)
                    make.width.equalTo((screenWidth / 2) - 26)
                    make.height.equalTo(30)
                    make.right.equalTo(self.contentView).offset(-13)
                }
            } else {
                titleLabel.snp.remakeConstraints { (make) in
                    make.bottom.equalTo(self.contentView).offset(-margin)
                    make.left.equalTo(self.contentView).offset(13)
                    make.width.greaterThanOrEqualTo(93)
                    if titleLabel.frame.size.width > 92 {
                        make.width.equalTo(titleLabel.frame.size.width)
                    }
                    make.height.equalTo(30)
                    make.right.equalTo(self.contentView).offset(-13)
                }
            }
        }
    }
    
    func selectedCell(selected: Bool) {
        if let titleLabel = self.titleLabel {
            if selected {
                titleLabel.textColor = UIColor.coopOrange
                titleLabel.font = UIFont.museo500Font(ofSize: 18)
                self.backgroundColor = UIColor.white
            } else {
                titleLabel.textColor = UIColor.coopQuantityGrey
                titleLabel.font = UIFont.museo500Font(ofSize: 18)
                self.backgroundColor = UIColor.transparentWhite
            }
        }
        self.layoutSubviews()
        self.setConstraints()
    }
}
