//
//  BarcodeViewController.swift
//  Coop Trainer App
//
//  Created by Rogier van den Brink on 10/05/2019.
//  Copyright © 2019 Coop. All rights reserved.
//

import UIKit
import AVFoundation

class BarcodeViewController: UIViewController {

    @IBOutlet var tabsView: TabsView?
    @IBOutlet var previewView: UIImageView?
    var session: AVCaptureSession?
    
    var openedScanner: Bool = false
    
    private var productViewController: UIViewController?
    
    override func viewDidLoad() {
        setup()
    }
    
    private func setup() {
        let session = AVCaptureSession()
        self.session = session
        
        session.sessionPreset = AVCaptureSession.Preset.photo
        let captureDevice = AVCaptureDevice.default(for: AVMediaType.video)
        
        let deviceInput = try! AVCaptureDeviceInput(device: captureDevice!)
        let deviceOutput = AVCaptureVideoDataOutput()
        deviceOutput.videoSettings = [kCVPixelBufferPixelFormatTypeKey as String: Int(kCVPixelFormatType_32BGRA)]
        deviceOutput.setSampleBufferDelegate(self, queue: DispatchQueue.global(qos: DispatchQoS.QoSClass.default))
        session.addInput(deviceInput)
        session.addOutput(deviceOutput)
        
        let previewView = UIImageView(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height))
        self.view.addSubview(previewView)
        self.previewView = previewView
        
        let barcodeImageView = UIImageView(frame: CGRect(x: (self.view.frame.size.width / 2) - 50, y: ((self.view.frame.size.height - 160) / 2) - 25, width: 100, height: 50))
        barcodeImageView.image = UIImage(named: "barcode")
        barcodeImageView.contentMode = .scaleAspectFill
        self.view.addSubview(barcodeImageView)
        
        let imageLayer = AVCaptureVideoPreviewLayer(session: session)
        imageLayer.frame = previewView.bounds
        imageLayer.videoGravity = .resizeAspectFill
        previewView.layer.addSublayer(imageLayer)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        startLiveVideo()
        self.openedScanner = false
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        stopSession()
    }
    
    func stopSession() {
        if let session = self.session,
            session.isRunning {
            DispatchQueue.global().async {
                session.stopRunning()
            }
        }
    }
    
    func restartSession() {
        if let session = self.session,
            session.isRunning {
            
            session.stopRunning()
            
            startLiveVideo()
        }
    }
    
    private func startLiveVideo() {
        
        if let session = self.session {
            self.configureMetaOutput()
            session.startRunning()
        }
    }
    
    fileprivate func configureMetaOutput() {
        //metadata detectors for local detection of barcode
        let metadataOutput = AVCaptureMetadataOutput()
        
        if let session = self.session,
            session.canAddOutput(metadataOutput) {
            session.addOutput(metadataOutput)
            
            metadataOutput.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
            metadataOutput.metadataObjectTypes = [.ean8, .ean13, .pdf417]
        }
    }
    
    func openProductScan(sku: String) {
        let viewController = ProductViewController()
        
        let closeView = StyleKit.closeIconView(frame: CGRect(x: 0, y: 0, width: 17, height: 17), color: UIColor.white)
        let renderer = UIGraphicsImageRenderer(size: closeView.bounds.size)
        let image = renderer.image { ctx in
            closeView.drawHierarchy(in: closeView.bounds, afterScreenUpdates: true)
        }
        let closeIconImage = image
        
        let done: UIBarButtonItem = UIBarButtonItem(image: closeIconImage, style: .done, target: self, action: #selector(self.hideScanner))
        viewController.navigationItem.setLeftBarButton(done, animated: false)
        
        let navController = UINavigationController(rootViewController: viewController)
        navController.view.backgroundColor = UIColor.white
        
        self.productViewController = navController
        
        self.present(navController, animated: true, completion: {
            //
            viewController.sku = sku
            viewController.isProduct = true
        })
    }
    
    @objc func hideScanner() {
        if let productViewController = self.productViewController {
            productViewController.dismiss(animated: true, completion: {
                //
            })
        }
    }
}

extension BarcodeViewController: AVCaptureVideoDataOutputSampleBufferDelegate {
    
    func captureOutput(_ output: AVCaptureOutput, didOutput sampleBuffer: CMSampleBuffer, from connection: AVCaptureConnection) {
        
        
    }
}

extension BarcodeViewController: AVCaptureMetadataOutputObjectsDelegate {
    
    func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {
        if let metadataObject = metadataObjects.first {
            guard let readableObject = metadataObject as? AVMetadataMachineReadableCodeObject else { return }
            guard let stringValue = readableObject.stringValue else { return }
            
            print("barcode: \(stringValue)")
            if !stringValue.isEmpty && !self.openedScanner {
                self.openedScanner = true
                self.openProductScan(sku: stringValue)
            }
        }
    }
}

