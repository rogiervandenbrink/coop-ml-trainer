//
//  FirestorageService.swift
//  Coop Trainer App
//
//  Created by Rogier van den Brink on 09/05/2019.
//  Copyright © 2019 Coop. All rights reserved.
//

import Foundation
import FirebaseStorage
import Firebase

class FirestorageService {
    
    var ref: DocumentReference? = nil
    
    let defaults = UserDefaults.standard
    
    init() {
        
    }
    
    func uploadVideoToStorage(fileURL: URL, uniqueId: String, isProduct: Bool, completionHandler: @escaping (Bool) -> Void) {
        
        let storage = Storage.storage()
        let storageRef = storage.reference()
        
        var videoRef = storageRef.child("productImages").child(uniqueId)
        if !isProduct {
            videoRef = storageRef.child("recipeImages").child(uniqueId)
        }
        
        let uploadTask = videoRef.putFile(from: fileURL, metadata: nil)
        uploadTask.observe(.resume) { snapshot in
            // Upload resumed, also fires when the upload starts
        }
        uploadTask.observe(.pause) { snapshot in
            // Upload paused
        }
        uploadTask.observe(.progress) { snapshot in
            let percentComplete = 100.0 * Double(snapshot.progress!.completedUnitCount)
                / Double(snapshot.progress!.totalUnitCount)
            // show alert with progress
            if percentComplete > 0 {
                Alert.setProgress(progress: Float(percentComplete / 100))
            }
        }
        uploadTask.observe(.success) { snapshot in
            // Upload completed successfully
            completionHandler(true)
        }
        uploadTask.observe(.failure) { snapshot in
            if let error = snapshot.error as? NSError {
                switch (StorageErrorCode(rawValue: error.code)!) {
                case .objectNotFound:
                    completionHandler(false)
                    print("File doesn't exist")
                    break
                case .unauthorized:
                    completionHandler(false)
                    print("User is not authorized")
                    break
                case .cancelled:
                    completionHandler(false)
                    print("User cancelled")
                    break
                case .unknown:
                    completionHandler(false)
                    print("Unknwon error occured")
                    break
                default:
                    // A separate error occurred. This is a good place to retry the upload.
                    uploadTask.observe(.resume) { snapshot in
                        // Upload resumed, also fires when the upload starts
                    }
                    break
                }
            }
        }
        
    }
    
    func addProductToFirestore(sku: String) {
        if let userEmail = defaults.string(forKey: Constants.emailAdress) {
            let db = Firestore.firestore()
            let data: [String: Any] = ["sku": sku]
        db.collection(Constants.usersMlTrainer).document(userEmail).collection(Constants.productsFilmed).document(sku).setData(data) { err in
                if let err = err {
                    print("Error writing document: \(err)")
                } else {
                    print("Document successfully written!")
                }
            }
        }
    }
    
    func countFilmedProducts(completionHandler: @escaping (Int) -> Void) {
        if let userEmail = defaults.string(forKey: Constants.emailAdress) {
            let db = Firestore.firestore()
            db.collection(Constants.usersMlTrainer).document(userEmail).collection(Constants.productsFilmed).getDocuments() { (querySnapshot, err) in
                if let err = err {
                    print("Error getting documents: \(err)")
                    completionHandler(0)
                } else {
                    let amount = querySnapshot!.count
                    print("amount of scanned products: \(amount)")
                    completionHandler(amount)
                }
            }
        } else {
            completionHandler(0)
        }
    }
    
    func checkAmount(completionHandler: @escaping (Int) -> Void) {
        if let userEmail = defaults.string(forKey: Constants.emailAdress) {
            let db = Firestore.firestore()
            db.collection(Constants.usersMlTrainer).document(userEmail).collection(Constants.productsFilmed).addSnapshotListener { documentSnapshot, error in
                guard let document = documentSnapshot else {
                    print("Error fetching document: \(error!)")
                    return
                }
                guard !document.isEmpty else {
                    print("Document data was empty.")
                    completionHandler(0)
                    return
                }
                completionHandler(document.count)
                print("Current data: \(document.count)")
            }
        } else {
            completionHandler(0)
        }
    }
    
    func addAmountFields(amount: Int) {
        if let userEmail = defaults.string(forKey: Constants.emailAdress) {
            let db = Firestore.firestore()
            let data: [String: Any] = [Constants.emailAdress: userEmail, Constants.productCount: amount]
            db.collection(Constants.usersMlTrainer).document(userEmail).setData(data)
        }
    }
    
    func checkIfAlreadyScanned(uniqueId: String, isProduct: Bool, completionHandler: @escaping (Bool) -> Void) {
        
        let storage = Storage.storage()
        let storageRef = storage.reference()
        
        var videoRef = storageRef.child("productImages").child(uniqueId).child("resized-frame0001.jpg")
        if !isProduct {
            videoRef = storageRef.child("recipeImages").child(uniqueId).child("resized-frame0001.jpg")
        }
        
        videoRef.getData(maxSize: 2 * 1024 * 1024) { (data, error) in
            if let error = error {
                print("check if product already exists: \(error.localizedDescription)")
                completionHandler(false)
            } else {
                completionHandler(true)
            }
        }
    }
}
